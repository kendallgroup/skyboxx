define(['knockout', 'text!./blocklist.html'], function(ko, htmlTemplate){

    var list = ko.observableArray([
        {
            header: "New Orders Today",
            blocktype: "dashboard-stat primary",
            value: "89",
            callback: function(){
                
            },
            icon: "fa fa-star",
            icon2: "fa fa-play-circle more"
        },
        {
            header: "Yesterdays Orders",
            blocktype: "dashboard-stat secondary",
            value: "74",
            callback: "func",
            icon: "fa fa-phone-square",
            icon2: "fa fa-shopping-cart more"
        },
        {
            header: "New Subscribers",
            blocktype: "dashboard-stat tertiary",
            value: "20",
            callback: "func",
            icon: "fa fa-paper-plane ",
            icon2: "fa fa-play-circle more"
        },
        {
            header: "Abandoned Carts",
            blocktype: "dashboard-stat",
            value: "45",
            callback: "func",
            icon: "fa fa-shield",
            icon2: "fa fa-eye more"
        },
    ]);

    function viewModel(){
        this.list = list;
    }

    return { viewModel: viewModel, template: htmlTemplate }
});