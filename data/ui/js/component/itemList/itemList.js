define(['knockout', 'text!./itemList.html'], function(ko, htmlTemplate){

    var table = ko.observableArray([
        {
            varDate: "09/21/2016",
            numOfItems: "12",
            price: "250",
            rowButton: "View",
            icon: "fa fa-chevron-right",
            url: "/"
        },
        {
            varDate: "09/21/2017",
            numOfItems: "4",
            price: "60",
            rowButton: "View",
            icon: "fa fa-chevron-right",
            url: "/"
        },
        {
            varDate: "09/21/2016",
            numOfItems: "6",
            price: "75",
            rowButton: "View",
            icon: "fa fa-chevron-right",
            url: "/"
        },
        {
            varDate: "09/21/2015",
            numOfItems: "5",
            price: "100",
            rowButton: "View",
            icon: "fa fa-chevron-right",
            url: "/"
        },
        {
            varDate: "09/21/2015",
            numOfItems: "10",
            price: "70",
            rowButton: "View",
            icon: "fa fa-chevron-right",
            url: "/"
        },
        {
            varDate: "09/21/2016",
            numOfItems: "8",
            price: "240",
            rowButton: "View",
            icon: "fa fa-chevron-right",
            url: "/"
        },
        {
            varDate: "09/21/2017",
            numOfItems: "5",
            price: "290",
            rowButton: "View",
            icon: "fa fa-chevron-right",
            url: "/"
        }
    ]);

    var headers = ko.observableArray([
        {
            headerNames: "Purchased On"
        },
        {
            headerNames: "Items"
        },
        {
            headerNames: "Amount"
        },
        {
            headerNames: ""
        }
    ]);

    function viewModel(){
        this.header = "Orders";
        this.headers = "";
        this.tableData = table;
        this.tableHeaders = headers;
    }

    viewModel.prototype.tableheaders = function(){
        //this will be used to parse data or convert data formats for the header names for this list
        return headers;
    }

    viewModel.prototype.someFunction = function(){
        //Will Allow you to manually create an order
        //there should be one page where all the odering is done.  This will forware to that page.
    }

    return { viewModel: viewModel, template: htmlTemplate }

});