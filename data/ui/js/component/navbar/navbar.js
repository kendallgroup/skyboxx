define(['knockout', 'text!./navbar.html'], function(ko, htmlTemplate){

    var menuItems = ko.observableArray([
        {
            singlemenuItem: true,
            dropdownItem: false,
            url: "../index.html",
            menuItemName: "Home",
            menuIcon: "fa fa-home"
        },
        {
            singlemenuItem: false,
            dropdownItem: true,
            url: "../index.html",
            menuItemName: "Reports",
            menuIcon: "fa fa-home",
            items: [
                {
                    menutext: "Daily Orders Report",
                    url: "../index.html",
                    icon: "fa fa-user"
                },
                {
                    menutext: "HeadCount",
                    url: "../index.html",
                    icon: "fa fa-calendar"
                },
                {
                    menutext: "Employee Schedule",
                    url: "../index.html",
                    icon: "fa fa-tasks"
                }
            ]
        }
    ]);

    

    function viewModel(){
        this.menuItems = menuItems;
    };

    return { viewModel: viewModel, template: htmlTemplate}
});