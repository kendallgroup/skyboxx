define(['knockout', 'text!./sidebar.html','jquery-ui'], function(ko, htmlTemplate){

    var menu = ko.observableArray([
        {
            type: "item",
            isDropDown: false,
            isMenuItem: true,
            url: "#",
            icon: 'fa fa-dashboard',
            displaytext: "DashBoard"
        },
        {
            type: "dropdown",
            isDropDown: true,
            isMenuItem: false,
            url: "#",
            icon: 'fa fa-dashboard',
            displaytext: 'Extra Pages',
            items:[
                {
                    url: "../html/page-login-require.html",
                    icon: "fa fa-unlock",
                    displaytext: "Login Basic"
                },
                { 
                    url: "../html/page-login-social.html",
                    icon: "fa fa-unlock",
                    displaytext: "Login Social"
                },
                {
                    url: "../html/page-404.html",
                    icon: "fa fa-ban",
                    displaytext: "404 Error"
                },
                {
                    url: "../html/page-404.html",
                    icon: "fa fa-ban",
                    displaytext: "500 Error"
                },
                {
                    url: "../html/page-blank.html",
                    icon: "fa fa-file-text-o",
                    displaytext: "Blank Page"
                }
            ]
        },
        {
            type: "dropdown",
            isDropDown: true,
            isMenuItem: false,
            url: "#",
            icon: 'fa fa-file-text',
            displaytext: 'Example Pages',
            items:[
                {
                    url: "../html/page-profile.html",
                    icon: "fa fa-user",
                    displaytext: "Login Basic"
                },
                { 
                    url: "../html/page-login-social.html",
                    icon: "fa fa-unlock",
                    displaytext: "Login Social"
                },
                {
                    url: "../html/page-invoice.html",
                    icon: "fa fa-money",
                    displaytext: "Invoice"
                },
                {
                    url: "../html/page-pricing.html",
                    icon: "ffa fa-dollar",
                    displaytext: "Pricing Plans"
                },
                {
                    url: "../html/page-support.html",
                    icon: "fa fa-picture-o",
                    displaytext: "Gallery"
                },
                {
                    url: "../html/page-settings.html",
                    icon: "fa fa-cogs",
                    displaytext: "Settings"
                },
                {
                    url: "../html/page-support.html",
                    icon: "fa fa-calendar",
                    displaytext: "Calendar"
                }
            ]
        },
        {
            type: "dropdown",
            isDropDown: true,
            isMenuItem: false,
            url: "#",
            icon: 'fa fa-tasks',
            displaytext: 'Form Elements',
            items:[
                {
                    url: "../html/form-regular.html",
                    icon: "fa fa-location-arrow",
                    displaytext: "Regular Elements"
                },
                { 
                    url: "../html/form-extended.html",
                    icon: "fa fa-magic",
                    displaytext: "Extended Elements"
                },
                {
                    url: "../html/form-validation.html",
                    icon: "fa fa-check",
                    displaytext: "Validation"
                }
            ]
        },
        {
            type: "dropdown",
            isDropDown: true,
            isMenuItem: false,
            url: "#",
            icon: 'fa fa-desktop',
            displaytext: 'UI Features',
            items:[
                {
                    url: "../html/page-ui-buttons.html",
                    icon: "fa fa-hand-o-up",
                    displaytext: "Buttons"
                },
                { 
                    url: "../ui-tabs.html",
                    icon: "fa fa-reorder",
                    displaytext: "Tabs & Accordions"
                },
                {
                    url: "../ui-popups.html",
                    icon: "fa fa-asterisk",
                    displaytext: "Popups / Notifications"
                },
                {
                    url: "../ui-sliders.html",
                    icon: "fa fa-tasks",
                    displaytext: "Sliders"
                },
                {
                    url: "../ui-typography.html",
                    icon: "fa fa-font",
                    displaytext: "Typography"
                },
                {
                    url: "../ui-icons.html",
                    icon: "fa fa-star-o",
                    displaytext: "Icons"
                },
                {
                    url: "../ui-portlets.html",
                    icon: "fa fa-list-alt",
                    displaytext: "Portlets"
                }
            ]
        },
        {
            type: "dropdown",
            isDropDown: true,
            isMenuItem: false,
            url: "#",
            icon: 'fa fa-table',
            displaytext: 'Tables',
            items:[
                {
                    url: "../table-basic.html",
                    icon: "fa fa-table",
                    displaytext: "Basic Tables"
                },
                { 
                    url: "./form-extended.html",
                    icon: "fa fa-table",
                    displaytext: "Advanced Tables"
                },
                {
                    url: "./form-validation.html",
                    icon: "fa fa-table",
                    displaytext: "Responsive Tables"
                }
            ]
        },
        {
            type: "dropdown",
            isDropDown: true,
            isMenuItem: false,
            url: "#",
            icon: 'fa fa-bar-chart-o',
            displaytext: 'Charts & Graphs',
            items:[
                {
                    url: "../chart-flot.html",
                    icon: "fa fa-bar-chart-o",
                    displaytext: " jQuery Flot"
                },
                { 
                    url: "./chart-morris.html",
                    icon: "fa fa-bar-chart-o",
                    displaytext: "Morris.js"
                }
            ]
        }
        
    ]);

    function viewModel(){
        this.menu = menu;
    };

    viewModel.prototype.callJqueryUI = function(){
        var mainnav = $('#main-nav'),
        openActive = mainnav.is ('.open-active'),
        navActive = mainnav.find ('> .active');

        mainnav.find ('> .dropdown > a').bind ('click', this.navClick);
        
        if (openActive && navActive.is ('.dropdown')) {			
            navActive.addClass ('opened').find ('.sub-nav').show();
        }
    };

    viewModel.prototype.navClick = function(e){
        e.preventDefault();
            
        var li = $(this).parents ('li');		
        
        if (li.is ('.opened')) { 
            //closeAll();	
            $('.sub-nav').slideUp().parents('li').removeClass('opened');		
        } else { 
            //closeAll();
            $('.sub-nav').slideUp().parents('li').removeClass('opened');
            li.addClass ('opened').find('.sub-nav').slideDown();			
        }
    };
    //Not sure what I was not able to call this function from navClick
    //will have to research this at a later date.  ****TODO LIST*****
    viewModel.prototype.closeAll = function(){
        $('.sub-nav').slideUp().parents('li').removeClass('opened');
    }
    

    return { viewModel: viewModel, template: htmlTemplate}
});