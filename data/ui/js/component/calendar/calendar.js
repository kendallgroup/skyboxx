define(['jquery','knockout', 'text!./calendar.html'], function($, ko, htmlTemplate){
    
    var date = new Date(),
    d = date.getDate(),
    m = date.getMonth(),
    y = date.getFullYear();

    
        
    function viewModel(){
        this.header = "Full Calendar - Require"
    }

    viewModel.prototype.callCalendar = function(){
        $('#full-calendar').fullCalendar({
            header: {
                left: 'prev,next',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            droppable: true,
            drop: function(date, allDay) { // this function is called when something is dropped
            
                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');
                
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);
                
                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                copiedEventObject.className = $(this).attr("data-category");
                
                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#full-calendar').fullCalendar('renderEvent', copiedEventObject, true);
                
                // is the "remove after drop" checkbox checked?
                //if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                //}
                
            },
    
            events: [
                {
                    title: 'All Day Event',
                    start: new Date(y, m, 1),
                    end: new Date (y, m, 2),
                    className: 'fc-red'
                },
                {
                    title: 'Long Event',
                    start: new Date(y, m, d-5),
                    end: new Date(y, m, d-2),
                    className: 'fc-yellow'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d-3, 16, 0),
                    allDay: false,
                    className: 'fc-grey'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d+1, 14, 0),
                    end: new Date(y, m, d+3, 14, 0),
                    allDay: false,
                    className: 'fc-charcoal'
                },
                {
                    title: 'Meeting',
                    start: new Date(y, m, d-13, 10, 30),
                    end: new Date (y,m,d-11, 10,30),
                    allDay: false,
                    className: 'fc-grey'
                },
                {
                    title: 'Lunch',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false,
                    className: 'fc-red'
                }
            ]
        });
    };


    return {viewModel: viewModel, template: htmlTemplate}
});