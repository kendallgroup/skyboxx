define(['knockout'], function(ko){

    var dropDownListItems = ko.observableArray([
        {
            url: "page-settings-require.html",
            displaytext: " Admin Settings",
            icon: "fa fa-cog"
        },
        {
            url: "page-support.html",
            displaytext: " Support Page",
            icon: "fa fa-calendar"
        },
        {
            url: "page-calendar.html",
            displaytext: " Schedule",
            icon: "fa fa-calendar"
        }, 
        {
            url: "page-profile.html",
            displaytext: " User Profile",
            icon: "fa fa-user"
        }
    ]);

  
    var viewModel = function(){
        this.dropdownitem = dropDownListItems;
        this.btnClass = "btn btn-default btn-sm dropdown-toggle";
        this.icon = "fa fa-info-circle";
        this.displaytext = "Change Week";
    };

    var htmlTemplate = `<div class="btn-group">
    <button class="" type="button" data-toggle="dropdown" data-bind="attr: { class: btnClass}">
    <i class="" data-bind="attr: { class: icon }"></i>  &nbsp;
    <span data-bind="text: displaytext"></span>
    <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu" data-bind="template: { name: 'dropdown_items', foreach: dropdownitem }">
        
    </ul>
    </div>
    
    <script type="text/html" id="dropdown_items">
        <li>
            <a href="#" data-bind="attr: { href: url}">
            <i class="" data-bind="attr: { class: icon}"></i>
            <span data-bind="text: displaytext"></span>
            </a>     
        </li>
    </script>
      `
    return { viewModel: viewModel, template: htmlTemplate }
});