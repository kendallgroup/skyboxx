define(['knockout'], function(ko){
    
    var widgetData = {
        header: "New Orders Today",
        blocktype: "dashboard-stat primary",
        value: "300",
        callback: "func",
        icon: "fa fa-star",
        icon2: "fa fa-play-circle more"
    };


    function viewModel(params){
        this.header = ko.observable(params.header ? params.header : widgetData.header);
        this.value = ko.observable(params.value ? params.value : widgetData.value);
        this.callback = ko.observable(params.callback ? params.callback : widgetData.callback);
        this.icon = ko.observable(params.icon ? params.icon : widgetData.icon);
        this.icon2 = ko.observable(params.icon2 ? params.icon2 : widgetData.icon2);
        this.blocktype = ko.observable(params.blocktype ? params.blocktype : widgetData.blocktype);
    }

    var htmlTemplate = `
    <div class="col-md-3 col-sm-6">

    <a href="" class="dashboard-stat" data-bind="click: callback, attr: { class: blocktype }">
        <div class="visual">
            <i class="fa fa-star" data-bind="attr: { class: icon }"></i>
        </div> <!-- /.visual -->

        <div class="details">
            <span class="content" data-bind="text: header"></span>
            <span class="value" data-bind="text: value"></span>
        </div> <!-- /.details -->

        <i class="fa fa-play-circle more" data-bind="attr: { class: icon2 }"></i>

    </a> <!-- /.dashboard-stat -->

</div> <!-- /.col-md-3 -->
    `

    return { viewModel: viewModel, template: htmlTemplate }
});