requirejs.config({
    baseUrl: "ui/js",
    paths: {
        'jquery'                : 'libs/jquery-1.9.1.min',
        'jquery-ui'             : 'libs/jquery-ui-1.9.2.custom.min',
        'text'                  : '../node_modules/requirejs-text/text',
        'knockout'              : '../node_modules/knockout/build/output/knockout-latest.debug',
        'bootstrap'             : 'libs/bootstrap',
        'raphael'               : 'libs/raphael-2.1.2.min',
        'jquery.icheck'         : 'plugins/icheck/jquery.icheck',
        'select2'               : 'plugins/select2/select2',
        'App'                   : 'App',
        'morris'                : 'plugins/morris/morris.min',
        'morris-area'           : 'charts/morris/area',
        'morris-donut'          : 'charts/morris/donut',
        'calendar'              : 'calendar',
        'dashboard'             : 'dashboard',
        'fullCalendar'          : 'plugins/fullcalendar/fullcalendar',
        'jquery.sparkline'      : 'plugins/sparkline/jquery.sparkline.min',
        'jquery.tableCheckable' : 'plugins/tableCheckable/jquery.tableCheckable',
        'moment'                : 'plugins/fullcalendar/moment',
        'jquery.flot'           : 'plugins/flot/jquery.flot',
        'magnific'              : 'plugins/magnific/jquery.magnific-popup',
        'alertify'              : 'alertify',
        'DataManager'           : '../../../js/app/Utilities/DataManager'
    },
    shim: {
        'morris-donut':{
            deps:['jquery', 'morris']
        },
        'morris-area':{
            deps:['jquery', 'morris']
        },
        'jquery-ui':{
            deps:['jquery'],
            exports: 'jQuery'
        },
        'bootstrap': {
            deps: ['jquery'],
            exports: 'jQuery'
        },
        'select2': {
            deps:['jquery']
        },
        'jquery.sparkline': {
            deps:['jquery'],
            exports: 'jQuery'
        },
        'morris': {
            deps:['jquery', 'raphael']
        },
        'jquery.tableCheckable': {
            deps:['jquery'],
            exports: 'jQuery'
        },
        'jquery.icheck': {
            deps:['jquery'],
            exports: 'jQuery'
        },
        'fullCalendar':{
            deps:['jquery', 'moment']
        },
        'jquery.flot':{
            deps:['jquery'],
            exports: 'jQuery'
        },
        'dashboard':{
            deps:['jquery', 'jquery.sparkline']
        },
        'calendar':{
            deps:['jquery', 'fullCalendar']
        },
        'App':{
            deps:['jquery']
        },
        'magnific':{
            deps:['jquery']
        },
        'DataManager':{
            deps:['jquery']
        }
    }
});

require(['jquery',
'knockout',
'text',
'moment',
'jquery.icheck',
'bootstrap',
'jquery.flot',
'dashboard',
'jquery-ui',
'jquery.tableCheckable',
'jquery.sparkline',
'fullCalendar',
'calendar',
'morris',
'morris-donut',
'morris-area',
'select2',
'raphael',
'App',
'magnific',
'alertify',
'DataManager'
], function ($, ko, text, varDate) {
    $(document).ready(function(){
        
        //Put this information in the database.  This will give clients access or denied access
        //to certain functionaility within ths site. 
        // var viewModel = function(){
        //     this.aboutpage = true;
        //     this.homepage = true;
        //     this.calltoaction = true;
        //     this.eventspage = true;
        //     this.gallery = true;
        //     this.testimonialspage = true;
        //     this.sectiondishes = true;
        //     this.openinghourspage = true;
        //     this.ourchefs = true;
        //     this.reservation = true;
        //     this.menupage = true;
        //     this.openinghourspage = true;
        // }


        ko.components.register('block', { require: 'widgets/infoBlock/infoBlock'});
        ko.components.register('blocklist', { require: 'component/blocklist/blocklist'});
        ko.components.register('itemlistwidget', { require: 'component/itemList/itemList'});
        ko.components.register('calendarpage', {require: 'component/calendar/calendar'});
        ko.components.register('navbar', { require: 'component/navbar/navbar'});
        ko.components.register('sidebar', { require: 'component/sidebar/sidebar'});
        ko.components.register('dropdown', { require: 'widgets/dropdown/dropdown'});
        //ko.components.register('adminLogin' , { require: 'pages/login/login'});

        ko.applyBindings();
    });     
});

