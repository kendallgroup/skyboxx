<?php 

class book_table extends DB\SQL\Mapper{

    public function __construct(DB\SQL $db){
        parent::__construct($db, 'book_table');
    }

    public function add(){
        $this->copyFrom('POST');
        $this->save();
    }
}