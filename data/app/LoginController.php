<?php

class LoginController extends Controller{

    public function render(){
        $this->f3->set('content','js/pages/login/login.html');
		echo View::instance()->render('layouts/Login_layout.html');
    }

    public function Login_user(){
        $username = $this->f3->get('POST.username');
        $pass = $this->f3->get('POST.password');

        $user = new User($this->db);
        $user_password = $user->alreadyExist($username);
       
        if (!$user_password){
            //$this->f3->set('context', 'login');
            $this->f3->set('content','js/pages/login/login.html');
            echo View::instance()->render('layouts/Login_layout.html');
        } else {
            $usersPassword = $user->getPassword($username);
            $isAuth = password_verify($pass, $usersPassword);
            if(!$isAuth){
                $this->logger->write($username . " failed valication check" , 'r');
                //$this->f3->set('context', 'login');
                $this->f3->set('content','js/pages/login/login.html');
                echo View::instance()->render('layouts/Login_layout.html');
                var_dump(http_response_code(401));
            } else {
                //Redirect to dashboard or Profile page
                $this->logger->write($user . " Logged in Successfully" , 'r');
                $this->f3->set('content','js/pages/home/home.html');
                echo View::instance()->render('layouts/layout.htm');
            }
        };
    }

    public function SetUser(){

    }

    public function getUser(){

    }
}

