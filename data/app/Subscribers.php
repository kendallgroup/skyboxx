<?php 

class Subscribers extends DB\SQL\Mapper {

    public function __construct(DB\SQL $db){
        parent::__construct($db, 'subscribers');
    }

    public function add($email){
        $this->copyFrom('POST');
        $this->save();
    }

    public function getAllSubscribers(){
        
    }
}