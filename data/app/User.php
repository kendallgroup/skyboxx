<?php
//User Model

class User extends DB\SQL\Mapper{

    public $User;

    public function __construct(DB\SQL $db){
        parent::__construct($db, 'users');
        //$User = parent::__construct($db, 'users');
    }

    public function add(){
        $this->copyFrom('POST');
        $this->save();
    }

    public function getById($id){
        $this->load(array('id=?', $id));
        return $this->query;
    }

    public function edit($id) {
	    $this->load(array('id=?',$id));
	    $this->copyFrom('POST');
	    $this->update();
	}

    public function alreadyExist($user){
        //$this->load(array('username=?', $user));
        $this->find(array('username=?', $user));
        return $this->dry();
    }

    public function getPassword($user){
        $username = $this->find(array('username=?', $user));
        $result = $username[0]->fields;
        $array = $result["password"]["value"];
        return $array;
    }

    public function delete($id) {
	    $this->load(array('id=?',$id));
	    $this->erase();
	}
}