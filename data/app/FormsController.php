<?php

class FormsController extends Controller{

    public function bookTable(){
        $table = new book_table($this->db);
        $table->add();
        if($table->dry()){
            echo false;
        } else {
            echo true;
        }
    }

    public function subscriber($params){
        $subscribe = new Subscribers($this->db);
        //**TODO** put a email check here -- Wide open right now.
        $subscribe->add($this->f3->get('POST.email_address'));
        if($subscribe->dry()){
            echo false;
        } else {
            echo true;
        }
    }
}