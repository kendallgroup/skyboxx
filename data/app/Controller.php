<?php 


class Controller{
    
    protected $f3;
    protected $db;
    public $logger;

    function afterroute(){
        //Can add any function before the route is routed
    }

    function beforeroute(){
        //Can add any function after the route is completed
    }

    function __construct(){
        $f3=Base::instance();
        $this->f3 = $f3;

        $db = new DB\SQL(
            $this->f3->get('devdb'),
            $this->f3->get('devuser'),
            $this->f3->get('devpass'),
            array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION)
        );
        $this->db = $db;

        $this->logger = new \Log('debug.log');
        $this->logger->write("Starting the Debugger -> This mean a controller was called");
    }
}