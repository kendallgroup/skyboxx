<?php

class DashboardController extends Controller{

    //Render Main Dashboard Page for Admin Section.
    public function render(){
        $this->f3->set('content', 'js/pages/home/home.html');
        echo View::instance()->render('layouts/layout.htm');
    }
}