<?php

$f3=require('lib/base.php');

if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');

$f3->config('config.ini');
$f3->config('routes.ini');

// *****<TODO>
// Create a Documentation Controller where all docs for the application will be avaiable in HTML format or PDF
//</TODO>

$f3->run();