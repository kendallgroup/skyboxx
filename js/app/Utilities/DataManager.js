define(['jquery','knockout'], function($, ko){

    function DataManager(){};
    //Object prepare for get command
    DataManager.prototype.dataObj = function ($url, $data){
        this.url = $url || {};
        this.data = $data || {};
    };

    DataManager.prototype.getData = function(dataObj){
        $.get(dataObj.url, dataObj.data).done(function(data){
            return data;
        }).fail(function(event){
            return data;
        });
    };

    DataManager.prototype.postData = function(dataObj){
        $.post(dataObj.url, dataObj.data).done(function(data){
            return data;
        }).fail(function(data){
            return data;
        });
    };

    DataManager.prototype.dataObjforAJAX = function(data){
        this.url = data.url;
        this.datatype = data.dataType || 'JSON';
        this.data = data.data || {};
        this.method = data.method || 'GET';
        this.processData = data.processData || true;
        this.statusCode = data.statusCode || {
            404: function() {
              alert( "page not found" );
            }
          };
    };

    DataManager.prototype.AJAX = function($data){
        var xhr = $.ajax({
            method: data.method,
            url: data.url,
            dataType: $data.dataType,
            data: data.data,
            processData: data.processData,
            statusCode: data.statusCode
        }).done(function($data){
            return $data;
        }).fail(function($data){
            return $data;
        });
    };

    return{
        dataObj : DataManager.dataObj,
        getData : DataManager.getData,
       postData : DataManager.postData,
        ajaxObj : DataManager.dataObjAJAX
    }
});