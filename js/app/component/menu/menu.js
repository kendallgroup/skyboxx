define(['knockout', 'text!./menu.html'], function(ko, htmlTemplate){
    
    // var menuItems = ko.observableArray([
    //     {
    //         url: "img/Menu1.jpeg",
    //         width: "180",
    //         class: "base",
    //         menuItemHeader: "Buffulo Wings",
    //         menuItemDescription: "Hot, Mild or Spicy",
    //         menuItemPrice: "$9.99"
    //     },
    //     {
    //         url: "img/Menu2.jpg",
    //         width: "180",
    //         class: "base",
    //         menuItemHeader: "Chicken Tenders",
    //         menuItemDescription: "Buffulo Sauce, Honey Mustard and Ranch",
    //         menuItemPrice: "$11.99"
    //     },
    //     {
    //         url: "img/Menu3.png",
    //         width: "180",
    //         class: "base",
    //         menuItemHeader: "Chicago Style Hot Dog",
    //         menuItemDescription: "Best Chicago Style outsite of ShyTown!",
    //         menuItemPrice: "$7.99"
    //     },
    //     {
    //         url: "img/Menu4.jpg",
    //         width: "180",
    //         class: "base",
    //         menuItemHeader: "Pizza",
    //         menuItemDescription: "Our Homemade Hand tossed Pizza with Toppings of your choice",
    //         menuItemPrice: "$24.99"
    //     },
    //     {
    //         url: "img/Menu5.jpg",
    //         width: "180",
    //         class: "base",
    //         menuItemHeader: "Skyboxx Signature Hamburger",
    //         menuItemDescription: "2 1/4 lb patties of angus beef topped with Lettuce, tomato, jalepeno and a fried egg.",
    //         menuItemPrice: "$12.99"
    //     },
    //     {
    //         url: "img/Menu6.jpg",
    //         width: "180",
    //         class: "base",
    //         menuItemHeader: "Mozorella Sticks",
    //         menuItemDescription: "Great Mozorella sticks",
    //         menuItemPrice: "$7.99"
    //     },
    //     {
    //         url: "img/Menu6.jpg",
    //         width: "180",
    //         class: "base",
    //         menuItemHeader: "Salmon blackend",
    //         menuItemDescription: "Blacked Salmon with Vegtable melody and Mashed patatoes",
    //         menuItemPrice: "$17.99"
    //     },
    //     {
    //         url: "img/Menu8.jpeg",
    //         width: "180",
    //         class: "base",
    //         menuItemHeader: "Roast and Veggies",
    //         menuItemDescription: "",
    //         menuItemPrice: "$24.99"
    //     },
    //     {
    //         url: "img/Menu9.jpg",
    //         width: "180",
    //         class: "base",
    //         menuItemHeader: "Rib Tips and Asparagus",
    //         menuItemDescription: "",
    //         menuItemPrice: "$26.99"
    //     },
    //     {
    //         url: "img/Menu10.jpg",
    //         width: "180",
    //         class: "base",
    //         menuItemHeader: "Shrimp and Veggies",
    //         menuItemDescription: "Fresh Shrimp and Veggies very tasty low calorie meal.",
    //         menuItemPrice: "$14.99"
    //     }
    // ]);

    var menuItems = ko.observableArray([
        {
            url: "img/Menu1.jpeg",
            price: "$14.99",
            productTitle: "Buffalo Wings",
            descript: "",
            buttonText: "Order Now"
        },
        {
            url: "img/Menu2.jpg",
            price: "$9.99",
            productTitle: "Chicken Tenders",
            descript: "",
            buttonText: "Buy Now"
        },
        {
            url: "img/Menu3.png",
            price: "$7.99",
            productTitle: "Chicago Style Hot Dog",
            descript: "",
            buttonText: "Order Online"
        },
        {
            url: "img/Menu5.jpg",
            price: "$7.99",
            productTitle: "Skyboxx Signature Hamburger",
            descript: "",
            buttonText: "Add to Cart"
        },
        {
            url: "img/Menu4.jpg",
            price: "$27.99",
            productTitle: "Pizza",
            descript: "",
            buttonText: "Buy Now"
        },
        {
            url: "img/Menu6.jpg",
            price: "$7.99",
            productTitle: "Mozorella Sticks",
            descript: "",
            buttonText: "Add to Cart"
        },
        {
            url: "img/Menu8.jpeg",
            price: "$18.99",
            productTitle: "Roast and Veggies",
            descript: "",
            buttonText: "Buy Now"
        },
        {
            url: "img/Menu9.jpg",
            price: "$32.99",
            productTitle: "Rib Tips and Asparagus",
            descript: "",
            buttonText: "Buy Now"
        },
        {
            url: "img/Menu10.jpg",
            price: "$32.99",
            productTitle: "Shrimp and Veggies",
            descript: "",
            buttonText: "Buy Now"
        }    
    ]);
    

    function viewModel(){
        this.header = "Skyboxx Menu";
        this.subHeader = "Testing"; //'Broswe our Tasty menu and <a href="#book-table">book-a-table</a> or order directly from our site <a href="#ordermenu"></a>';
        this.menuItem = menuItems;
    }

    viewModel.prototype.shoppingCart = function(){
        $('body').ctshop({
            currency: '$',
            paypal: {
              currency_code: 'USD'
            }
        });
    }

    return {viewModel: viewModel, template: htmlTemplate}
});