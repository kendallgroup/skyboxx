define(['knockout', 'text!./gallery.html'], function(ko, htmlTemplate){

    var gallerylist = ko.observableArray([
      { 
        url: 'img/specialK.jpg', 
        urlsource: 'img/specialK.jpg', 
        classes: 'fa fa-search-plus'
      },
      { 
        url: 'img/specialK-skyboxxgallery.jpg', 
        urlsource: 'img/specialK-skyboxxgallery.jpg', 
        classes: 'fa fa-search-plus'
      },
      { 
        url: 'img/skyboxxcomedynightgallery.jpg',
        urlsource: 'img/skyboxxcomedynightgallery.jpg', 
        classes: 'fa fa-search-plus'
      },
      { 
        url: 'img/SPecialKEarnestSMithgallery.jpg', 
        urlsource: 'img/SPecialKEarnestSMithgallery.jpg', 
        classes: 'fa fa-search-plus'
      },
      { 
        url: 'img/crowdgallery.jpg', 
        urlsource: 'img/crowdgallery.jpg', 
        classes: 'fa fa-search-plus'
      },
      { 
        url: 'img/skyboxgallery.jpg', 
        urlsource: 'img/skyboxgallery.jpg', 
        classes: 'fa fa-search-plus'
      },
      { 
        url: 'img/skyboxgallery1.jpg', 
        urlsource: 'img/skyboxgallery1.jpg', 
        classes: 'fa fa-search-plus'
      },
      { 
        url: 'img/skyboxgallery4.jpg', 
        urlsource: 'img/skyboxgallery4.jpg', 
        classes: 'fa fa-search-plus'
      }
    ]);

    function viewModel(){
        this.header = "Gallery";
        this.subHeading = "Skyboxx First Gallery";
        this.gallerylist = gallerylist;
    }

    return { viewModel: viewModel, template: htmlTemplate}
});