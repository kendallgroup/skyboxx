define(['knockout', 'text!./chefs.html'], function(ko, htmlTemplates){
    
    var chefs = ko.observableArray([
        { 
            url: 'img/skyboxxChefs1.jpeg', 
            facebook: '#', 
            twitter: '#',
            linkedin: '#',
            chefname: 'Jason Moore',
            jobtitle: 'Chef',
            quote: 'I put love in what I do!  Welcome to Skyboxx',
            
          },
          { 
            url: 'img/skyboxxChefs2.jpeg', 
            facebook: '#', 
            twitter: '#',
            linkedin: '#',
            chefname: 'Jay Jay Evans',
            jobtitle: 'Master Chef',
            quote: 'Looking forward to cooking for you!!',
            
          },
          { 
            url: 'img/skyboxxChefs1.jpeg', 
            facebook: '#', 
            twitter: '#',
            linkedin: '#',
            chefname: 'Stephen Holmes',
            jobtitle: 'Assitant Chef',
            quote: 'Come see us at the Skyboxx',
            
          }
    ]);
    
    function viewModel(){
        this.header = 'Our Chefs';
        this.subheading = 'Here are our award winning Chefs';
        this.chefs = chefs;
    }

    return {viewModel: viewModel, template: htmlTemplates}
});