define(['knockout','text!./aboutus.html'], function(ko, htmltemplate){
    //console.log(template);

    var aboutList = ko.observableArray([
      { url: 'img/aboutSkyboxxBuilding.jpeg', 
        header: 'Morrow GA', 
        subHeading: 'Location conveniently of InterState 75'
      },
      { url: 'img/AboutSkyboxxBoxing.jpg', 
        header: 'Sporting Events', 
        subHeading: 'We have all your sports needs with Boxing, UFC and your Weekly games on TV'
      },
      { url: 'img/barImageAboutSkyboxx.jpg',
        header: 'Best Drinks in Town', 
        subHeading: 'We have a Great staff waiting to serve you at the bar!!'
      },
      { url: 'img/aboutSkyboxxComedy.jpg', 
        header: 'Come Laugh With Us', 
        subHeading: 'We have Some of the best Comedian in Atlanta And DJ\'s twice a week!'
      }
  ]);

    function viewModel(params){
        this.header = "About SkyBoxx";
        this.subHeading = "Your Place for Sports and comradery.  Cold Drinks at the Bar And on the Weekend you light up the dance floor ";
        this.list = aboutList;
    }
    var varDate = Date.parse('12/14/2017');
    console.log(varDate)
    return { viewModel: viewModel, template: htmltemplate}
});