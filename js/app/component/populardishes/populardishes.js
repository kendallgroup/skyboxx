define(['knockout', 'text!./populardishes.html','owl-carousel'], function(ko, htmlTemplate){

    var list = ko.observableArray([
      { 
        url: 'img/Menu4.jpg', 
        price: '$17.99', 
        menuItemName: 'Pizza',
        description: 'Deep Dish Pizza made to perfection........'
      },
      { 
        url: 'img/Menu1.jpeg', 
        price: '$14.99', 
        menuItemName: 'Buffulo Wings - 20 pc',
        description: 'Hot, Mild, Honey Mustard, Lemon Pepper'
      },
      { 
        url: 'img/Menu2.jpg', 
        price: '$12.99', 
        menuItemName: 'Chicken Tenders',
        description: 'Hot, Mild and Honey Mustard'
      },
      { 
        url: 'img/Menu3.png', 
        price: '$5.99', 
        menuItemName: 'Chicago Hot Dog',
        description: 'Chicago Style Hot Dog'
      },
      { 
        url: 'img/Menu5.jpg', 
        price: '$9.99', 
        menuItemName: 'Skyboxx Burger',
        description: 'This is Skyboxx Signature item on the menu'
      },
      { 
        url: 'img/Menu6.jpg', 
        price: '$7.99', 
        menuItemName: 'Mozarella Sticks',
        description: 'Great Apetizer'
      }

    ]);

    function viewModel(){
        this.header = "Popular Dishes";
        this.subHeading = "Checkout our best dishes";
        this.poplist = list;
    }

    viewModel.prototype.initOwl = function(){
        $("#dishes-slider").owlCarousel({
            loop:true,
            margin:0,
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        });
    }

    return { viewModel: viewModel, template: htmlTemplate}
});