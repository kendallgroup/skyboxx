define(['knockout', 'text!./navbar.html'], function(ko, htmlTemplate){
    var internalLinks = ko.observableArray([
        {
            navName: 'Home', 
            navLink: 'home'
        },
        {
            navName: 'About', 
            navLink: 'about'
        },
        {
            navName: 'Menu', 
            navLink: 'menu'
        },
        {
            navName: 'Events', 
            navLink: 'events'
        },
        {
            navName: 'Gallery', 
            navLink: 'gallery'
        },
        {
            navName: 'Book a Table', 
            navLink: 'book-table'
        }
    ]);

    var otherLinks = ko.observableArray([
        {
            linkType : 'modal',
            target : '#contact-modal',
            navName: 'Contact'
        }
    ]);


    function viewModel(){
        this.brandName = "Skyboxx";
        this.url = "#";
        this.intNav = internalLinks;
        this.extNav = otherLinks;
    }

    viewModel.prototype.close_toggle = function(){
        if ($(window).width() <= 768) {
            $('.navbar-collapse a').on('click', function () {
                $('.navbar-collapse').collapse('hide');
            });
        }
        else {
            $('.navbar .navbar-default a').off('click');
        }
        close_toggle();
        
        $(window).resize(close_toggle);
    }

    return {viewModel: viewModel, template: htmlTemplate }
});