define(['knockout', 'text!./calltoaction.html'], function(ko, htmlTemplate){
    function viewModel(){
        this.header = "Get Food and Event discounts",
        this.subscribeMessage = "Subscribe To Our Email List"
    }

    viewModel.prototype.emailSubscriber = function(){
        $('#emailSubmit').on("click", function(e){
            e.preventDefault();
            var emailaddress = $('#inlineFormInputGroup').val();
            var customerName = $('#customer_name').val();
            var url = 'data/api/subscribe'; 
            var data = { email_address : emailaddress, customer_name: customerName};
            var dataType = 'json';
    
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: dataType
              })
              .done(function(data){
                    console.log(data);
                    console.log(data.email);
                        $('#inlineFormInputGroup').val('');
                        $('.success').html('<div class="alert alert-success"><strong>Success!</strong> Indicates a successful or positive action.</div>');
                        $('#subscribe').modal('toggle');
                        
              })
              .fail(function(data){
                  console.log("failed");
                  console.log(data.responseText);
                  console.log(data.status);
                  console.log(data.statusText)
                  console.log(data);
              });
        });
    }

    return { viewModel: viewModel, template: htmlTemplate}
});