define(['knockout', 'text!./booking.html'], function(ko, htmlTemplate){
    function viewModel(){
        this.header = "Book a table";
        this.subHeading = "Looking forward to you Joining us!!";
        this.method = ""; //POST
        this.action=""; ///api/book_table/
    };

    viewModel.prototype.pickerDate = function(){
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    };

    viewModel.prototype.makeReservation = function(){
        this.getData();
    };

    viewModel.prototype.getData = function(){
        this.formatDate();
        $('#book-a-table').on('click', function(e){
            e.preventDefault();
            var newForm = $('.reservation-form').serialize();
            $.ajax({
                type: "POST",
                url: "data/api/booking",
                data: newForm,
                dataType: "JSON"
            })
            .done(function(data){
                $('.reservation-form').find("input, textarea").val("");
                $('#thankyou').modal('show');       
            })
            .fail(function(data){
                console.log("failed");
                console.log(data);
                alert('ajax failed');
            });
        });
    }

    viewModel.prototype.formatDate = function(){
        var vardate = $('#datepicker').val();
        var vartime = $('#vartime').val();
        var formatdate = vardate + ' ' + vartime;
        $('#datepicker').val(formatdate);
        //return formatdate;
    };

    return { viewModel: viewModel, template: htmlTemplate}
});