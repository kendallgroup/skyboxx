define(['knockout', 'text!./testimonials.html'], function(ko, htmlTemplate){

    var list = ko.observableArray([
        { url: 'img/testimonialGirlPic.jpg', 
          subHeading: 'I really Enjoyed the Admosphere here at SkyBoxx and I am in love with the Skyboxx Burger.Thumbs up!!', 
          customer: 'Rhonda Rousey -'
        },
        { url: 'img/testimonialSistaPic.jpg', 
          subHeading: 'They had a DJ and Karoke after the game.....Once Again You have facilitated Some very special memories.  Now What you got on my next Drink.... ', 
          customer: 'Jason Tatum - '
        },
        { url: 'img/testimonialsQueenPic.jpg', 
          subHeading: 'They had a DJ and Karoke after the game.....Once Again You have facilitated Some very special memories.  Now What you got on my next Drink.... ', 
          customer: 'Ashley Turner - '
        }
    ]);

    var viewModel = function(){
        this.header = "WHAT THEY SAY";
        this.subHeading = "There is What some of our customer say about us!";
        this.varList = list;
        this.wowDelay = '.2s';
    }

    viewModel.prototype.slider = function(){
      $('.testi-slider').flexslider({
          animation: "slide",
          direction: "horizantal",
          directionNav: true,
          controlNav: false,
          prevText: "<i class='fa fa-angle-left'></i>",
          nextText: "<i class='fa fa-angle-right'></i>"
      });
    }

    return { viewModel: viewModel, template: htmlTemplate}
});