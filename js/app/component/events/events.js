define(['knockout', 'text!./events.html'], function(ko, htmlTemplate){

    var eventlist = ko.observableArray([
      { 
        url: 'img/skyboxevents2.jpg', 
        eventNameOverlay: 'Holloween Costume Party', 
        eventHeaderOverlay: '$100 to the Best Costume',
        eventName: 'Holloween Costume Party',
        eventdate: '26 October 2017',
        eventtime: '8:00 PM',
        buttontext: 'View Details',
        link: '#event1',
        eventTagline: 'Best Costume Party In the City......'
      },
      { 
        url: 'img/skyboxevents5.jpg', 
        eventNameOverlay: 'New Years Party', 
        eventHeaderOverlay: 'Celebrate New Years with Us!!!!',
        eventName: 'New Years',
        eventdate: '14 December 2017',
        eventtime: '10:00 PM',
        buttontext: 'View Details',
        link: '#event2',
        eventTagline: 'Come party with your favorite Bar and grill!'
      },
      { 
        url: 'img/AboutSkyboxxBoxing.jpg', 
        eventNameOverlay: 'Holloween Costume Party', 
        eventHeaderOverlay: '$100 to the Best Costume',
        eventName: 'The Big Fight',
        eventdate: '26 August 2017',
        eventtime: '11:30 PM',
        buttontext: 'View Details',
        link: '#event3',
        eventTagline: 'This is the biggest fighting event of our lifetime.....See you at Skyboxx'
      }
    ]);

    function viewModel(){
        this.header = "Upcoming events";
        this.subHeading = "Checkout our upcoming events";
        this.events = eventlist;
    };

    return { viewModel: viewModel, template: htmlTemplate}
});