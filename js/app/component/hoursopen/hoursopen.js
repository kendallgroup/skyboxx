define(['knockout', 'text!./hoursopen.html'], function(ko, htmlTemplate){
    function viewModel(){
        this.header = "Opening hours";
        this.subHeading = "Call For Reservations or <a href='#book-table'>Book at table</a>";
        this.weekend = "Friday to Sunday";
        this.weekendHours = "10:00 AM - 2:00 AM";
        this.weekday = "Monday to Thursday";
        this.weekHours = "05:00 AM - 11:00 PM";
        this.tel = "Call us: 770-471-9191";
    }

    return { viewModel: viewModel, template: htmlTemplate}
});