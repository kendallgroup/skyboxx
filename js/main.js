requirejs.config({
    baseUrl: "js",
    paths: {
        'jquery'                : 'jquery.min',
        'jquery-ui'             : 'jquery-ui.min',
        'text'                  : 'node_modules/requirejs-text/text',
        'knockout'              : 'node_modules/knockout/build/output/knockout-latest.debug',
        'bootstrap'             : '../css/bootstrap/js/bootstrap.min',
        'core'                  : 'video-background/core',
        'transition'            : 'video-background/transition',
        'background'            : 'video-background/background',
        'contact_me'            : 'contact_me',
        'jqBootstrapValidation' : 'jqBootstrapValidation',
        'restaurant-custom'     : 'restaurant-custom',
        'lightbox2'             : 'lightbox2/dist/js/lightbox.min',
        'wow'                   : 'wow.min',
        'pace'                  : 'pace.min',
        'owl-carousel'          : 'owl-carousel/owl.carousel.min',
        'jquery.flexslider'     : 'jquery.flexslider-min',
        'jquery.backstretch'    : 'jquery.backstretch.min',
        'jquery.easing'         : 'jquery.easing.1.3.min',
        'datejs'                : 'date',
        'shop'                  : 'shop'
    },
    shim: {
        'jquery-ui':{
            deps:['jquery'],
            exports: 'jQuery'
        },
        'bootstrap': {
            deps: ['jquery'],
            exports: 'jQuery'
        },
        'jqBootstrapValidation': {
            deps:['jquery'],
            exports: 'jQuery'
        },
        'lightbox2': {
            deps:['jquery'],
            exports: 'jQuery'
        },
        'owl-carousel': {
            deps:['jquery'],
            exports: 'jQuery'
        },
        'jquery.flexslider': {
            deps:['jquery'],
            exports: 'jQuery'
        },
        'jquery.backstretch': {
            deps:['jquery'],
            exports: 'jQuery'
        },
        'background': {
            deps:['jquery'],
            exports: 'jQuery'
        },
        'jquery.easing': {
            deps:['jquery'],
            exports: 'jQuery'
        },
        'core': {
            deps:['jquery'],
            exports: 'jQuery'
        },
        'transition': {
            deps:['jquery'],
            exports: 'jQuery'
        },
        'restaurant-custom':{
            deps:['jquery', 'jquery-ui']
        },
        'shop':{
            deps:['jquery']
        }

    }
});

require(['jquery',
'knockout',
'text',
'date',
'jquery-ui',
'jquery.easing',
'jquery.backstretch',
'jquery.flexslider',
'owl-carousel',
'wow',
'pace',
'core',
'transition',
'background',
'restaurant-custom',
'bootstrap',
'jqBootstrapValidation',
'lightbox2',
'shop'
], function ($, ko, text) {
    $(document).ready(function(){
         // Formstone Background - Video Background Settings
         $(".video-section").background({
            source: {
                poster: "img/drink_cover.jpg",
                mp4: "img/drink.mp4"
            }
        });

        var viewModel = function(){
            this.aboutpage = true;
            this.homepage = true;
            this.calltoaction = true;
            this.eventspage = true;
            this.gallery = true;
            this.testimonialspage = true;
            this.sectiondishes = true;
            this.openinghourspage = true;
            this.ourchefs = true;
            this.reservation = true;
            this.menupage = true;
            this.openinghourspage = true;
        }

        // Registering Knockout Component Modules
        //Resigter Site Navigation
        ko.components.register('navbar', { require: 'app/component/navbar/navbar'});
        // Registering About us Component for application
        ko.components.register('aboutus', { require: 'app/component/aboutus/aboutus'});
        // Register Testimonials Component Module
        ko.components.register('testimonials', { require: 'app/component/testimonials/testimonials'});
        // Register Open hours Module
        ko.components.register('hoursopen', { require: 'app/component/hoursopen/hoursopen'});
        // Register Popular Dishes Module
        ko.components.register('populardishes', { require: 'app/component/populardishes/populardishes'});
        // Register Call To Action Module
        ko.components.register('calltoaction', { require: 'app/component/calltoaction/calltoaction'});
        //Register Events Module
        ko.components.register('events', { require: 'app/component/events/events'});
        //Register Chef Module
        ko.components.register('our-chefs', { require: 'app/component/chefs/chefs'});
        //Register Gallery Module
        ko.components.register('gallery', { require: 'app/component/gallery/gallery'});
        //Register Book Table
        ko.components.register('booking', { require: 'app/component/booking/booking'});
        //Register Menu for Site
        ko.components.register('skyboxxmenu', {require: 'app/component/menu/menu'});
        
        //Knockout Components
        ko.applyBindings(viewModel);
    });     
});

